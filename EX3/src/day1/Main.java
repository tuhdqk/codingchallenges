package day1;
import java.util.*;
public class Main {
	public static void main(String[] args) {
	      
        Scanner l =new Scanner(System.in);
        Queue<Character> operator;
        Queue<Integer> operands;

        System.out.println("Enter calculations");
        String ca= l.nextLine();
       
        operands=new LinkedList<>();
        operator=new LinkedList<>();
        int res=0,c;
        for(int i=0 ; i<ca.length() ;i++){
            c =ca.charAt(i);
            if(!isOperator(c)){
                res *= 10;
                res += c - '0';
            }else{
                operator.add((char)c);
                operands.add(res);
                res=0;
            }
        }
        operands.add(res);
    
        calculations< Queue<Character>,  Queue<Integer>> cacu= new calculations< Queue<Character>, Queue<Integer>>(operands,operator);
        cacu.print();
        l.close();
    }
    public static boolean isOperator(int c) {
        return c == '+' || c == '-' || c == '*' || c == '\\';
    }
}
