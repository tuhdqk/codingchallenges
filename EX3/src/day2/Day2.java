package day2;
import java.util.ArrayList;

import java.util.Scanner;
public class Day2 {

	public static TaskImp taskImp = new TaskImp();
	static ArrayList<User> arrUser = new ArrayList<>();

	public static void MenuClient(ArrayList<Task> arrTask) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Menu");
		System.out.println("1. Add ");
		System.out.println("2. Update ");
		System.out.println("3. Delete ");
		System.out.println("4. Search ");
		System.out.println("5. Display ");
		System.out.println("6. Sort Arr Task ");
		System.out.println("7. Sign out ");
		System.out.println("0. Exit ");
		System.out.println("Enter your choice: ");
		int ch = Integer.parseInt(sc.nextLine());
		switch (ch) {
		case 0:
			break;
		case 1:
			taskImp.Input_Arr(arrTask);
			MenuClient(arrTask);
			break;
		case 2:
			taskImp.Update_Task(arrTask);
			MenuClient(arrTask);
			break;
		case 3:
			taskImp.Delete_Task(arrTask);
			MenuClient(arrTask);
			break;
		case 4:
			taskImp.Search_by_TaskTitle(arrTask);
			MenuClient(arrTask);
			break;
		case 5:
			taskImp.DisplayAllTask(arrTask);
			MenuClient(arrTask);
			break;
		case 6:
			taskImp.SortArrTask(arrTask);
			MenuClient(arrTask);
			break;
		case 7:
			Login(arrTask);
			break;
		default:
			System.out.println("Incorrect input format!");
			Login(arrTask);
			break;
		}
	}

	public static void MenuVisitor(String name, ArrayList<Task> arrTask) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Menu");
		System.out.println("1. Display my task ");
		System.out.println("2. Update ");
		System.out.println("3. Sign out");
		System.out.println("0. Exit ");
		System.out.println("Enter your choice: ");
		int ch = Integer.parseInt(sc.nextLine());
		switch (ch) {
		case 0:
			break;
		case 1:
			taskImp.DisplayByAssign(name, arrTask);
			MenuVisitor(name, arrTask);
			break;
		case 2:
			taskImp.Update_Task(arrTask);
			MenuVisitor(name, arrTask);
			break;
		case 3:
			Login(arrTask);
			break;
		default:
			System.out.println("Incorrect input format!");
			Login(arrTask);
			break;
		}
	}

	public static void Login(ArrayList<Task> arrTask) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Welcom to my project! Please log in first!");
		boolean tt = true;
		do {
			System.out.print("Username: ");
			String userString = sc.nextLine();
			System.out.print("Password: ");
			String passString = sc.nextLine();
			for (int i = 0; i < arrUser.size(); i++) {
				if (userString.equals(arrUser.get(i).getUsername())
						&& passString.equals(arrUser.get(i).getPassword())) {
					if (arrUser.get(i).getPosition().equals("Client")) {
						MenuClient(arrTask);
					} else if (arrUser.get(i).getPosition().equals("Visitor")) {
						MenuVisitor(arrUser.get(i).getUsername(), arrTask);
					}
				} else {
					tt = false;
				}
			}
			if (tt = false) {
				System.out.println("I can't find your account. Please try again!");
			}
		} while (tt = false);
	}

	public static void main(String[] args) {
		ArrayList<Task> arrTask = new ArrayList<>();
		arrUser.add(new User("nickname1", "123", "Client"));
		arrUser.add(new User("nickname2", "123", "Visitor"));
		arrUser.add(new User("nickname3", "123", "Client"));
		Login(arrTask);
	}

}