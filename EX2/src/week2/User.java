package week2;

import java.util.*;

public class User {
	int userId;
	String userName, password, email;

	ArrayList<Integer> favourite = new ArrayList<>();

	public User() {
		super();
	}

	public User(int userId, String userName, String password, String email,
			ArrayList<Integer> favourite) {
		super();
		this.userName = userName;
		this.password = password;
		this.email = email;
		this.userId = userId;
		this.favourite = favourite;
	}

	public User(String lineString) {
		String [] line = new String[5];
		line = lineString.split(" # ");
		this.userId = Integer.parseInt(line[0]);
		this.userName = line[1];
		this.password = line[2];
		this.email = line[3];
		favourite = GanListFbook(line[4]);
		
	}

	public ArrayList<Integer> GanListFbook(String lisF) {

		String[] lisFbook = lisF.split(" / ");
		for (int i = 0; i < lisFbook.length; i++) {
			favourite.add(Integer.parseInt(lisFbook[i]));
		}

		return favourite;
	}

	public String toStringListFavoritebook() {
		String t = favourite.get(0).toString();
		for (int i = 1; i < favourite.size(); i++) {
			t = t + " / " + favourite.get(i).toString();
		}
		return t;
	}

	public String toStringFile() {
		return userId + " # " + userName + " # " + password + " # " + email + " # " + toStringListFavoritebook();
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public ArrayList<Integer> getFavourite() {
		return favourite;
	}

	public void setFavourite(ArrayList<Integer> favourite) {
		this.favourite = favourite;
	}

}
