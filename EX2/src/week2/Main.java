package week2;

import java.util.*;
import java.io.*;

public class Main extends Thread {
	static ArrayList<User> _User;
 	static ArrayList<Book> _ArrBook;
	static UserImp user_ = new UserImp();
	public static void Login(ArrayList<User> user, ArrayList<Book> arrBook)  {
		Scanner sc = new Scanner(System.in);
	
		boolean tt = true;
		do {
			System.out.print("Username: ");
			String userString = sc.nextLine();
			System.out.print("Password: ");
			String passString = sc.nextLine();
		

			for (int i = 0; i < user.size(); i++) {
				if (userString.equals(user.get(i).getUserName()) && passString.equals(user.get(i).getPassword())) {
					user_.LogFile(userString+" has log in");
					user_.Menu(arrBook, user, userString);
				} else {
					tt = false;
					
				}
			}
			if (tt == false) {
				user_.LogFile("wrong user or password for user: "+userString);
				System.out.println("404");
			}
		} while (tt == false);
		sc.close();
	}

	public ArrayList<User> ReadFileUser(ArrayList<User> ListUser) {
		String filename = "Data.txt";
		try {
			File file = new File(filename);
			if (!file.exists()) {
				file.createNewFile();
			}

			try (BufferedReader bReader = new BufferedReader(new FileReader(filename))) {
				String lineString = bReader.readLine();
				while (lineString != null) {
					User userAtributes = new User(lineString);
					ListUser.add(userAtributes);
					lineString = bReader.readLine();
				} 
			}
		} catch (Exception e) {
		}
		return ListUser;
	}

	public ArrayList<Book> ReadFileBook(ArrayList<Book> ListBook) {
		String filename = "DataBook.txt";
		try {
			File file = new File(filename);
			if (!file.exists()) {
				file.createNewFile();
			}

			try (BufferedReader bReader = new BufferedReader(new FileReader(filename))) {
				String lineString = bReader.readLine();
				while (lineString != null) {
					Book bookAttributes = new Book(lineString);
					ListBook.add(bookAttributes);
					lineString = bReader.readLine();
				}
			}
		} catch (Exception e) {
		}
		return ListBook;
	}
	
	public static void main(String[] args) throws SecurityException, IOException {
		Main week2 = new Main();

		ArrayList<Book> arrBoookAtributes = new ArrayList<>();
		arrBoookAtributes = week2.ReadFileBook(arrBoookAtributes);
		_ArrBook=arrBoookAtributes;
		

		
		user_.LogFile("Start");
		
		ArrayList<User> arruserAtributes = new ArrayList<>();
		arruserAtributes = week2.ReadFileUser(arruserAtributes);
		_User=arruserAtributes;
		week2.start();
		
	
	}

	

	@Override
	public void run() {
		Login(this._User, this._ArrBook);
		
	}

}
