package week2;

import java.util.*;

public class MagicOfBooks {
	Scanner scanner = new Scanner(System.in);

	public void AddBook(ArrayList<Book> arrBook) {
		String tmpString = "";
		do {
			int idbook = arrBook.size() + 1;
			System.out.println("BOOK  " + idbook);
			System.out.println("Enter  name: ");
			String nameBook = scanner.nextLine();
			System.out.println("Enter  author: ");
			String Authorname = scanner.nextLine();
			System.out.println("Enter  description: ");
			String description = scanner.nextLine();
			Book book = new Book(idbook, nameBook, Authorname, description);
			arrBook.add(book);
			tmpString = scanner.nextLine();
		} while (tmpString.trim().equals(""));
	}

	public void DisplayBook(ArrayList<Book> arrBook) {
		for (int i = 0; i < arrBook.size(); i++) {
			System.out.println(arrBook.get(i).toString());
		}
	}

	public void SearchbyID(ArrayList<Book> arrBook, int id) {
		for (int i = 0; i < arrBook.size(); i++) {
			if (arrBook.get(i).getBookId() == id) {
				System.out.println(arrBook.get(i).getBookId() + " - " + arrBook.get(i).getBookName());
			}
		}
	}

	public void DisplayBookbyID(ArrayList<Book> arrBook, int id) {
		for (Book book : arrBook) {
			if (book.getBookId() == id) {
				System.out.println(book.toString());
			}
		}
	}

}
