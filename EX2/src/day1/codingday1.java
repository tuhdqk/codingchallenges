package day1;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;

public class codingday1 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		int n;
		do {
			System.out.println("Enter the number of elements in the array: ");
			n = scanner.nextInt();
		} while (n < 0);
		int array[] = new int[n];
		System.out.println("Enter elements for array: ");
		try {
			for (int i = 0; i < n; i++) {
				System.out.print("Enter the th element " + i + ": ");
				array[i] = scanner.nextInt();
			}
		} catch (InputMismatchException e) {
			System.out.println("Error!"+e);
		}
			
		
		System.out.println("\nInitial array: ");
		for (int i = 0; i < n; i++) {
			System.out.print(array[i] + "\t");
		}
		codingday1 aCodingday1=new codingday1();
		aCodingday1.EX(array);
		System.out.println("======================");
		Random random = new Random();
		int r = random.nextInt(n - 1);
		System.out.println("The value random: " + array[r]+"--"+"position: "+r);
	}
	public void EX(int [] array) {
		Scanner scanner = new Scanner(System.in);
		try {
			System.out.println("Enter divisor: ");
			int a = scanner.nextInt();

			for (int i = 5; i >= 0; i--) {
				double b = array[i] / a;
				System.out.println("The value of array is:" + b);
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Array is out of Bounds" + e);
		} catch (ArithmeticException e) {
			System.out.println("Can't divide by Zero" + e);
		}
		
	}
	
}