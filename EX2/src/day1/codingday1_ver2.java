package day1;

import java.util.Random;
import java.util.Scanner;

public class codingday1_ver2 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		int n;
		do {
			System.out.println("Enter the number of elements in the array: ");
			n = scanner.nextInt();
		} while (n < 0);
		int array[] = new int[n];

		System.out.println("Enter elements for array: ");
		for (int i = 0; i < n; i++) {
			System.out.print("Enter the th element " + i + ": ");
			array[i] = scanner.nextInt();
		}
		System.out.println("\nInitial array: ");
		for (int i = 0; i < n; i++) {
			System.out.print(array[i] + "\t");
		}
		try {
			System.out.println("Enter divisor: ");
			int a = scanner.nextInt();

			for (int i = 5; i >= 0; i--) {
				array[i] = array[i] / a;
				System.out.println("The value of array is" + array[i]);
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Array is out of Bounds" + e);
		} catch (ArithmeticException e) {
			System.out.println("Can't divide by Zero" + e);
		} catch (Exception e) {
			System.out.println("Error" + e);
		}
		System.out.println("======================");
		Random random = new Random();
		int r = random.nextInt(n - 1);
		System.out.println("The value random" + array[r]);
	}
	
}
