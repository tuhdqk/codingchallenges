package day4;

import java.util.Iterator;
import java.util.NavigableSet;
import java.util.TreeSet;

public class TreeSetImplementation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Creating a NavigableSet object  with
	      // reference to TreeSet class
	        NavigableSet<String> ts = new TreeSet<>();
	 
	        // add elements to treeSet object
	        ts.add("Geek");
	        ts.add("For");
	        ts.add("Geeks");
	 
	         // print the treeSet
	        System.out.println("Tree Set is " + ts);
	 
	        //print the elements using iterator interface
	        Iterator<String> valueIterator=ts.iterator();
	        System.out.println("The string values are: ");
	        while (valueIterator.hasNext()) {
				System.out.println(valueIterator.next());
				
			}
	        
	        // Print the first element in
	        // the TreeSet
	        System.out.println("First Value " + ts.first());
	 
	        // Print the last element in
	        // the TreeSet
	        System.out.println("Last Value " + ts.last());
	        
	        //retrieve and remove the last element
	        ts.pollLast();
	        System.out.println("Remove the last element successful!");
	        
	        //print the size of the treeSet
	        System.out.println("The size of the treeSet: "+ts.size());
	}

}
