package day4;

import java.util.PriorityQueue;
import java.util.Scanner;

public class QueueImplementation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PriorityQueue<Integer> pQueue = new PriorityQueue<Integer>();
		// Adds elements {0, 1, 2, 3, 4, .... , n} to
		// Add elements in the priorityQueue
		int n;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of elements to add: ");
		n = sc.nextInt();
		for (int i = 0; i < n; i++)
			pQueue.add(i);
		// print the top element of the priority queue
		int top = pQueue.poll();
		System.out.println("the top element of the priority queue: " + top);
		// print the size of the queue
		int size = pQueue.size();
		System.out.println("Size of queue: " + size);
		// print the head of the queue
		int head = pQueue.peek();
		System.out.println("head of queue: " + head);
		// delete an element of the queue
		int removedele = pQueue.remove();
		System.out.println("removed element: " + removedele);
		// print the queue elements
		System.out.println("Elements of queue " + pQueue);
		// remove all the elements of the queue at once
		boolean removeAll = pQueue.removeAll(pQueue);

		// check if the queue is empty or not
		if (removeAll == true) {
			System.out.println("The queue is empty");
		} else {
			System.out.println("The queue is not empty");
		}

	}

}
