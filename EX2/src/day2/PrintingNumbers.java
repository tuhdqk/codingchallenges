package day2;

import java.util.concurrent.atomic.AtomicInteger;

class even extends Thread {
	@Override
	public void run() {
		for (int i = 1; i <= 20; i++) {
			if (i%2==0) {
				System.out.println("even Number: " + i);
			}
			
		}
	}

}

class odd extends Thread {

	@Override
	public void run() {
		for (int i = 1; i <= 20; i++) {
			if(i%2!=0) {
				System.out.println("odd Number: " + i);
			}
			
		}
	}

}

public class PrintingNumbers {
	public static void main(String[] args) {
		Thread t1 = new Thread(new even());
		Thread t2 = new Thread(new odd());
		t1.start();
		t2.start();
	}
}