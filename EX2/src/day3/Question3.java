package day3;

import java.util.Scanner;
import java.util.Stack;

public class Question3 {
	Stack<Integer> stacks = new Stack<>();
	Scanner sc = new Scanner(System.in);

	public void AddElements() {
		String tt = "";
		do {
			System.out.print("Enter the number of arr: ");
			int n = sc.nextInt();
			System.out.println("Enter eplement in arr!");
			for (int i = 0; i < n; i++) {
				System.out.print("Arr[" + (i + 1) + "] = ");
				while (!sc.hasNextInt()) {
					System.out.println("That's not a Integer! Please try again!");
					System.out.print("Arr[" + (i + 1) + "] = ");
					sc.next();
				}
				int x = sc.nextInt();
				stacks.push(x);
			}
		} while (tt.equals("Y"));
	}

	public void DisplayStack() {
		for (int i = 0; i < stacks.size(); i++) {
			System.out.println("Arr[ " + (i + 1) + "] = " + stacks.get(i));
		}
	}

	public void Iterator() {
		java.util.Iterator<Integer> iterator = stacks.iterator();
		int i = 0;
		while (iterator.hasNext()) {
			Integer valueInteger = iterator.next();
			System.out.println("Arr[" + (i + 1) + "] = " + valueInteger);
			i++;
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Question3 ex3 = new Question3();
		ex3.AddElements();
		System.out.println("=========================================");
		// Display the Stack
		ex3.DisplayStack();
		System.out.println("=========================================");
		// Display the values using the iterator
		ex3.Iterator();
	}

}
