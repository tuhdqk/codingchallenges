package day3;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
public class Question1 {
	ArrayList<Integer> arrIntegers = new ArrayList<>();
	ArrayList<Integer> arr = new ArrayList<>();
	Scanner sc = new Scanner(System.in);
	int sizeArr;
	public void InputInt() {
		String tt = "";
		do {
			System.out.print("Enter the number of arr: ");
			int n = sc.nextInt();
			System.out.println("Enter eplement in arr!");
			for(int i = 0; i<n; i++) {
				System.out.print("Arr["+(i+1)+"] = ");
				int x = sc.nextInt();
				arrIntegers.add(x);
				arr.add(x);
			}
		} while (tt.equals("Y"));
	}
	private void Display() {
		System.out.println("2- Display arr");
		for(int i =0; i < arrIntegers.size(); i++) {
			System.out.println("Arr[ "+(i+1) +"] = "+ arrIntegers.get(i));
		}
	}
	private void DeleteByIndex() {
		System.out.print("The position you want to remove in arr:  ");
		int index = sc.nextInt();
		if (arrIntegers.remove(index - 1) != null) {
			System.out.println("Deleted successful");
		}
		else {
			System.out.println("Not found element!");
		}
		Display();
	}
	private void DeleteByObject() {
		System.out.print("The element you want to remove in arr:  ");
		int index = sc.nextInt();
		if (arrIntegers.remove(Integer.valueOf(index))) {
			System.out.println("Deleted successful");
		}
		else {
			System.out.println("Not found element!");
		}
		Display();
	}
	public void CheckSizeArr() {
		System.out.println("ArrayList before delete!");
		int index = 0;
		for(int i : arr) {
			System.out.println("Arr["+(index +1)+"] = "+ i);
			index++;
		}
		System.out.println("ArrayList after delete!");
		for(int i : arrIntegers) {
			System.out.println("Arr["+(index +1)+"] = "+ i);
			index++;
		}
	}
	public int MinElement () {
		arrIntegers.sort((o1, o2) -> o1-o2);
		int min = arrIntegers.get(0);
		return min;
	}
	public int MaxElement () {
		arrIntegers.sort((o1, o2) -> o1-o2);
		int max = arrIntegers.get(arrIntegers.size() -1);
		return max;
	}
	public void PintArrByEnhancedForLoop() {
		System.out.println("Print Arr use enhanced For loop!");
		int index = 0;
		for(int i : arrIntegers) {
			System.out.println("Arr["+(index +1)+"] = "+ i);
			index++;
		}
	}
	public static void main(String[] args) {
		Question1 day3 = new Question1();
//		1 - Add elements in the arrayList
		day3.InputInt();
//		2 - Print the size of the arrayList
		System.out.println("=========================================");
		day3.Display();
//		3 - Remove the arrayList element using index
		System.out.println("=========================================");
		day3.DeleteByIndex();
//		4 - Remove the arrayList element by passing the value in remove method
		System.out.println("=========================================");
		day3.DeleteByObject();
//		5 - Check the size of the arrayList
		System.out.println("=========================================");
		day3.CheckSizeArr();
//		6 - Find the min element in the arrayList
		System.out.println("=========================================");
		System.out.println("Min element in arr: " + day3.MinElement());
//		7 - Find the max element in the arrayList
		System.out.println("Max element in arr: " + day3.MaxElement());
//		8 - Print all the elements using an enhanced for loop
		System.out.println("=========================================");
		day3.PintArrByEnhancedForLoop();
		
	}

}
