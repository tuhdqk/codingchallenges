package day3;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Scanner;
public class Question2 {
	Scanner sc = new Scanner(System.in);

	public void InputInt(LinkedList<Integer> llInt) {
		String tt = "";
		do {
			System.out.print("Enter the number of arr: ");
			int n = sc.nextInt();
			System.out.println("Enter eplement in arr!");
			for (int i = 0; i < n; i++) {
				System.out.print("Arr[" + (i + 1) + "] = ");
				while (!sc.hasNextInt()) {
					System.out.println("That's not a Integer! Please try again!");
					System.out.print("Arr[" + (i + 1) + "] = ");
					sc.next();
				}
				int x = sc.nextInt();
				llInt.add(x);
			}
//			int i = 0;
//			while(sc.hasNextInt() && i < n) {
//				System.out.print("Arr["+(i+1)+"] = ");
//				
//				i++;
//			}
		} while (tt.equals("Y"));
	}

	private static void Display(LinkedList<Integer> llInt) {
		System.out.println("2- Display arr");
		for (int i = 0; i < llInt.size(); i++) {
			System.out.println("Arr[ " + (i + 1) + "] = " + llInt.get(i));
		}
	}

	public static void main(String[] args) {
		Question2 ex2 = new Question2();
		LinkedList<Integer> llInt = new LinkedList<>();
		ex2.InputInt(llInt);
		System.out.println("=========================================");
		Display(llInt);
		System.out.println("=========================================");
		System.out.println();
		System.out.println("LinkedList increasing");
		Collections.sort(llInt, new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				// TODO Auto-generated method stub
				return o1 < o2 ? -1 : 1;
			}
		});
		Display(llInt);
		System.out.println("=========================================");
		System.out.println("LinkedList decreasing");
		Collections.sort(llInt, Collections.reverseOrder());
		Display(llInt);
	}
}
