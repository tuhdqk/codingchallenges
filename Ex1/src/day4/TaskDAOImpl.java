package day4;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import java.util.Scanner;

public class TaskDAOImpl implements TaskDAO{

	Scanner scanner = new Scanner(System.in);
	//ArrayList<Task> arrTasks = new ArrayList<>();

	@Override
	public void Input_Arr(ArrayList<Task> arrTasks) {
		String status = "";
		do {
			Task newTask = new Task();
			newTask.Input_Task(arrTasks);
			arrTasks.add(newTask);
			System.out.println("Enter to exits!");
			status = scanner.nextLine();
		} while (!status.trim().equals(""));
	}
	
	@Override
	public void DisplayAllTask(ArrayList<Task> arrTasks) {
		for(int i = 0; i< arrTasks.size();i++) {
			System.out.println(arrTasks.get(i).toString());
		}
	}
	
	@Override
	public void DisplayByAssign(String name, ArrayList<Task> arrTasks) {
		int tmp = 0;
		for(int i = 0; i< arrTasks.size(); i++) {
			if (arrTasks.get(i).getTaskassignedTo().equals(name)) {
				System.out.println(arrTasks.get(i).toString());
				tmp++;
			}
		}
		if (tmp == 0 ) {
			System.out.println("Sorry! I can't find your task! Please try another!");
		}
	}
	
	@Override
	public void Update_Task(ArrayList<Task> arrTasks) {
		int tmp = 0;
		String statuString = "Y";
		do {
			System.out.print("Enter TaskID need to update: ");
			int TaskID = Integer.parseInt(scanner.next());
			for (int i = 0; i < arrTasks.size(); i++) {
				if (arrTasks.get(i).getTaskID() == TaskID) {
					System.out.print("Enter Task Title: ");
					arrTasks.get(i).setTaskTitle(scanner.next());
					System.out.print("\n Enter Task Text: ");
					arrTasks.get(i).setTaskText(scanner.next());
					System.out.print("Enter Assigned to: ");
					arrTasks.get(i).setTaskassignedTo(scanner.next());
					while (true) {
					System.out.print("Enter the date: ");
					String date = scanner.nextLine();
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
					dateFormat.setLenient(false);
					try {
						arrTasks.get(i).setDateText(dateFormat.parse(date));
						if (date.length() == 0) {
							throw new Exception("Date can't null! Please try again!");
						}
						else break;
					} catch (ParseException e) {
						System.out.println("Date have to follow the format day/month/year! Try again!");
					} catch (Exception e) {
						e.printStackTrace();
					}}
					System.out.print("Enter Task status: ");
					arrTasks.get(i).setTaskstatus(scanner.nextLine());
					tmp = 1;
					break;
				}
			}
			if (tmp == 0) {
				System.out.println("I can't find the Task have ID - " + TaskID);
			} else {
				System.out.println("Update successful!!");
			}
			System.out.println("Do you want to continuous? Enter to exit!");
			statuString = scanner.nextLine();
		} while (!statuString.trim().equals(""));
	}

	@Override
	public void Delete_Task(ArrayList<Task> arrTasks) {
		int tmp = 0;
		String statuString = "Y";
		do {
			System.out.print("Enter TaskID need to delete: ");
			int TaskID = Integer.parseInt(scanner.next());
			for (int i = 0; i < arrTasks.size(); i++) {
				if (arrTasks.get(i).getTaskID() == TaskID) {
					arrTasks.remove(i);
					tmp = 1;
					break;
				}
			}
			if (tmp == 0) {
				System.out.println("I can't find the Task have ID - " + TaskID);
			} else {
				System.out.println("Delete successful!!");
			}
			System.out.println("Do you want to continuous? Enter to exit!");
			statuString = scanner.nextLine();
		} while (!statuString.trim().equals(""));
	}

	@Override
	public void Search_by_TaskTitle(ArrayList<Task> arrTasks) {
		int tmp = 0;
		String statuString = "Y";
		do {
			System.out.print("Enter TaskTitle you want to search: ");
			String TaskTitle = scanner.nextLine();
			for (int i = 0; i < arrTasks.size(); i++) {
				if (arrTasks.get(i).getTaskTitle().toLowerCase().trim().equals(TaskTitle.toLowerCase().trim())) {
					System.out.println(arrTasks.get(i).toString());
					tmp = 1;
					break;
				}
			}
			if (tmp == 0) {
				System.out.println("I can't find the Task have title - " + TaskTitle);
			} else {
				System.out.println("Search successful!!");
			}
			System.out.println("Do you want to continuous? Enter to exit!");
			statuString = scanner.nextLine();
		} while (!statuString.trim().equals(""));
		
	}
	
	@Override
	public void SortArrTask(ArrayList<Task> arrTasks) {
		int c;
        boolean check = true;
        do {
            System.out.println("Sort by: 0. Decreasing / 1. Increasing");
            c = scanner.nextInt();
            if(c >= 0 && c <= 1) check = false;
            else {
                System.out.println("Enter the value 0 or 1!");
            }
        }while(check);
        Sort(c, arrTasks);
        DisplayAllTask(arrTasks);
    }
    public void Sort (int i, ArrayList<Task> arr) {
        Collections.sort(arr, new Comparator<Task>() {
 
            @Override
            public int compare(Task t1, Task t2) {
                if(i == 0) {
                    return (t1.getTaskTitle().compareTo(t2.getTaskTitle()));
                }
                else {
                    return (t2.getTaskTitle().compareTo(t1.getTaskTitle()));
                }
            }
        });
	}


}
