package day4;

import java.util.ArrayList;
import java.util.Scanner;

import day4.Client;
import day4.Task;
import day4.User;
import day4.Visitor;

public class Day4 {

	public static ArrayList<User> arrUser = new ArrayList<>();
	public static void Login(ArrayList<Task> arrTask) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Welcom to my project! Please log in first!");
		boolean tt = true;
		do {
			System.out.print("Username: ");
			String userString = sc.nextLine();
			System.out.print("Password: ");
			String passString = sc.nextLine();
			for (int i = 0; i < arrUser.size(); i++) {
				if (userString.equals(arrUser.get(i).getUsername())
						&& passString.equals(arrUser.get(i).getPassword())) {
					if (arrUser.get(i).getPosition().equals("Client")) {
						Client c = new Client();
						c.MenuClient(arrTask);
					} else if (arrUser.get(i).getPosition().equals("Visitor")) {
						Visitor v = new Visitor();
						v.MenuVisitor(arrUser.get(i).getUsername(), arrTask);
					}
				} else {
					tt = false;
				}
			}
			if (tt = false) {
				System.out.println("I can't find your account. Please try again!");
			}
		} while (tt = false);
	}

	public static void main(String[] args) {

		ArrayList<Task> arrTask = new ArrayList<>();
		arrUser.add(new User("nickname1", "123", "Client"));
		arrUser.add(new User("nickname2", "123", "Visitor"));
		arrUser.add(new User("nickname13", "123", "Client"));
		Login(arrTask);
	}

}