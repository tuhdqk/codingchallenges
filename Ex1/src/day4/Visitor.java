package day4;

import java.util.ArrayList;
import java.util.Scanner;

public class Visitor {
	public  TaskDAOImpl taskImp = new TaskDAOImpl();
	public void MenuVisitor(String name, ArrayList<Task> arrTask) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Menu");
		System.out.println("1. Display my task ");
		System.out.println("2. Update ");
		System.out.println("3. Sign out");
		System.out.println("0. Exit ");
		System.out.println("Enter your choice: ");
		int ch = Integer.parseInt(sc.nextLine());
		switch (ch) {
		case 0:
			break;
		case 1:
			taskImp.DisplayByAssign(name, arrTask);
			MenuVisitor(name, arrTask);
			break;
		case 2:
			taskImp.Update_Task(arrTask);
			MenuVisitor(name, arrTask);
			break;
		case 3:
			Day4 ngay4 = new Day4();
			ngay4.Login(arrTask);
			break;
		default:
			Day4 d4 = new Day4();
			System.out.println("Incorrect input format!");
			d4.Login(arrTask);
			break;
		}
	}

}
