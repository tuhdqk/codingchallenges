package day1;

import java.util.Iterator;
import java.util.Scanner;
import java.util.Arrays;

public class lesson1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("My name is Vu Van Tu");
		// Input the number of task
		System.out.println("Enter the number of task: ");
		int t = Integer.parseInt(sc.nextLine());
		// Create array
		String[] tasks = new String[t];
		// Input String to array
		for (int i = 0; i < tasks.length; i++) {
			System.out.print("Task " + (i + 1) + ": ");
			tasks[i] = sc.nextLine();
		}

		System.out.println("=============increasing==============");
		// Print array
		int c = 0;
		while (c < tasks.length) {
			System.out.println("Task " + (c + 1) + ": " + tasks[c]);
			c++;
		}

		System.out.println("==============decreasing=============");
		//
		for (int i = tasks.length - 1; i >= 0; i--) {
			System.out.println("Task " + (i + 1) + ": " + tasks[i]);
		}
		System.out.println("=============repeated==============");
		for (int i = 0; i < tasks.length; i++) {
			boolean check = true;
			int count = 1;
			if (i == 0)
				check = true;
			else {
				for (int k = 0; k < i; k++) {
					if (tasks[i].toLowerCase().equals(tasks[k].toLowerCase())) {
						check = false;
						break;
					}
				}
			}
			if (check == true) {
				for (int j = i + 1; j < tasks.length; j++) {
					if (tasks[i].toLowerCase().equals(tasks[j].toLowerCase())) {
						count++;
					}
				}
				System.out.println(tasks[i] + " " + count + " times");
			}
		}
		for (int i = 0; i < tasks.length - 1; i++) {
			for (int j = i + 1; j < tasks.length; j++) {
				if (tasks[i].compareTo(tasks[j]) > 0) {
					String temp = tasks[i];
					tasks[i] = tasks[j];
					tasks[j] = temp;
				}

			}
		}

		System.out.println("=============Sort by A-Z==============");
		for (String a : tasks) {
			System.out.print(a + "\t");
		}
	}

}
